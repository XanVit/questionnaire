<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Entity\User;

class SecurityController extends AbstractController
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @Route("/login", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', [
            'last_username' => $lastUsername,
            'error' => $error]);
    }

    /**
     * @Route("/logout", name="app_logout", methods={"GET"})
     */
    public function logout()
    {
        return $this->urlGenerator->generate('app_login');
    }

    /**
     * @Route("/registration", name="app_signup")
     */
    public function signup()
    {
        return $this->render('security/signup.html.twig');
//
    }

    /**
     * @Route("/registrredir", name="app_regredir")
     */
    public function registr()
    {
        $entityManager = $this->getDoctrine()->getManager();

        $user = new User;
        $user->setEmail($_POST['email'])
            ->setPassword($this->passwordEncoder->encodePassword($user, $_POST['password']));
        $entityManager->persist($user);
        $entityManager->flush();
        return $this->redirectToRoute("app_login");
    }
}
