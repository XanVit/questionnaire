<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 26.03.19
 * Time: 14:28
 */

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Question;

class AdminController extends AbstractController
{
    /**
     * @Route("/admin", name="app_admin")
     */
    public function show()
    {
        return $this->render('admin/menu.html.twig');
    }

    /**
     * @Route("/admin/newqu", name="admin_create")
     */
    public function questionEdit()
    {
        return $this->render('admin/questform.html.twig');
    }

    /**
     * @Route("/admin/newqured", name="admin_createred")
     */
    public function flushQuestion()
    {
        $entityManager = $this->getDoctrine()->getManager();

        $question = new Question;
        $question->setDescription($_POST['description'])
            ->setAnswer($_POST['answer'])
            ->setIdQuestionnaire($_POST['idqu']);
        $entityManager->persist($question);
        $entityManager->flush();
        return $this->redirectToRoute("app_admin");
    }
}