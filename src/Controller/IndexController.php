<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 26.03.19
 * Time: 14:05
 */

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class IndexController extends AbstractController
{
    /**
     * @Route("/", name="app_main")
     */
    public function show()
    {
        $role = null;
        if($this->getUser()){
            $role = explode('_',($this->getUser()->getRoles()[0]))[1];
        }
        else $role = null;
        return $this->render('main/main.html.twig',['role'=>$role]);
    }
}